// clasa MonomIntreg mosteneste Monom si defineste conceptul de coeficient real

public class MonomReal extends Monom {
	private Double coeficient;
	
	public MonomReal(double coeficient, int grad) {
		super(grad);
		this.coeficient = coeficient;
	}
	
	public Double getCoeficient() {
		return coeficient.doubleValue();
	}

	public void setCoeficient(Number coeficient) {
		this.coeficient = coeficient.doubleValue();
	}
	
	public String toString(int gradMax) {
		switch(super.getGrad()) {
		case 0:
			if(gradMax == 0) {
				if(coeficient < 0)
					return (-1)*coeficient + "";
				else
					return coeficient + "";
			}
			else {
				if(coeficient < 0)
					return "- " + (-1)*coeficient;
				else
					return "+ " + coeficient;
			}
		case 1:
			if(gradMax == 1) {
				if(coeficient == 1)
					return "x ";
				else
				if(coeficient == -1)
					return "x ";
				else
					if(coeficient < 0)
						return (-1)*coeficient + "x ";
					else
						return coeficient + "x ";
			}
			else {
				if(coeficient == 1)
					return "+ " + "x ";
				else
				if(coeficient == -1)
					return "- " + "x ";
				else
					if(coeficient < 0)
						return "- " + (-1)*coeficient + "x ";
					else
						return "+ " + coeficient + "x ";
			}
				
		default:
			if(super.getGrad() == gradMax){
				if(coeficient == 1)
					return "x^" + super.getGrad() + " ";
				else
					if(coeficient == -1)
						return "-x^" + super.getGrad() + " "; 
					else
						return coeficient + "x^" + super.getGrad() + " ";
			}
			else {
				if(coeficient == 1)
					return "+ "  + "x^" + super.getGrad() + " ";
				else
					if(coeficient == -1)
						return "- " + "x^" + super.getGrad() + " ";
					else
						if(coeficient < 0)
							return "- " + (-1)*coeficient + "x^" + super.getGrad() + " ";
						else
							return "+ " + coeficient + "x^" + super.getGrad() + " ";
			}
		}
	}
	
}
