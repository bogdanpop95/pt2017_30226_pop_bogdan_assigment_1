import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JUnitTest {

	private static Polinom P;
	private static Polinom Q;
	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		P = new Polinom();
		Monom m1 = new MonomIntreg(3, 4);
		Monom m2 = new MonomIntreg(4, 3);
		Monom m3 = new MonomIntreg(3, 1);
		Monom m4 = new MonomIntreg(1, 0);
		P.adaugaMonom(m1);
		P.adaugaMonom(m2);
		P.adaugaMonom(m3);
		P.adaugaMonom(m4);
		P.initPolinom();
		Q = new Polinom();
		Monom m5 = new MonomIntreg(-2, 0);
		Monom m6 = new MonomIntreg(3, 2);
		Monom m7 = new MonomIntreg(4, 1);
		Q.adaugaMonom(m5);
		Q.adaugaMonom(m6);
		Q.adaugaMonom(m7);
		Q.initPolinom();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("S-au executat " + nrTesteExecutate + " teste din care "+ nrTesteCuSucces + " au avut succes!");
	}
	
	@Before
	public void setUp() throws Exception {
		nrTesteExecutate++;
	}
	
	@Test
	public void testAdunare() {
		Polinom asteptat = new Polinom();
		Monom m1 = new MonomIntreg(3,4);
		Monom m2 = new MonomIntreg(4,3);
		Monom m3 = new MonomIntreg(3,2);
		Monom m4 = new MonomIntreg(7,1);
		Monom m5 = new MonomIntreg(-1,0);
		asteptat.adaugaMonom(m1);
		asteptat.adaugaMonom(m2);
		asteptat.adaugaMonom(m3);
		asteptat.adaugaMonom(m4);
		asteptat.adaugaMonom(m5);
		Polinom rezultat = Operatie.Adunare(P, Q);
		assertTrue(rezultat.comparaPolinom(asteptat)); // verifica daca adunarea este corecta
		nrTesteCuSucces++;
	}
	
	@Test
	public void testScadere() {
		Polinom asteptat = new Polinom();
		Monom m1 = new MonomIntreg(3,4);
		Monom m2 = new MonomIntreg(4,3);
		Monom m3 = new MonomIntreg(-3,2);
		Monom m4 = new MonomIntreg(-1,1);
		Monom m5 = new MonomIntreg(3,0);
		asteptat.adaugaMonom(m1);
		asteptat.adaugaMonom(m2);
		asteptat.adaugaMonom(m3);
		asteptat.adaugaMonom(m4);
		asteptat.adaugaMonom(m5);
		Polinom rezultat = Operatie.Scadere(P, Q);
		assertTrue(rezultat.comparaPolinom(asteptat)); // verifica daca scaderea este corecta
		nrTesteCuSucces++;
	}
	
	@Test
	public void testInmultire() {
		Polinom asteptat = new Polinom();
		Monom m1 = new MonomIntreg(9,6);
		Monom m2 = new MonomIntreg(24,5);
		Monom m3 = new MonomIntreg(10,4);
		Monom m4 = new MonomIntreg(1,3);
		Monom m5 = new MonomIntreg(15,2);
		Monom m6 = new MonomIntreg(-2,1);
		Monom m7 = new MonomIntreg(-2,0);
		asteptat.adaugaMonom(m1);
		asteptat.adaugaMonom(m2);
		asteptat.adaugaMonom(m3);
		asteptat.adaugaMonom(m4);
		asteptat.adaugaMonom(m5);
		asteptat.adaugaMonom(m6);
		asteptat.adaugaMonom(m7);
		Polinom rezultat = Operatie.Inmultire(P, Q);
		assertTrue(rezultat.comparaPolinom(asteptat)); // verifica daca inmultirea este corecta
		nrTesteCuSucces++;
	}
	
	@Test
	public void testImpartire() {
		Polinom catAsteptat = new Polinom();
		Polinom restAsteptat = new Polinom();
		Polinom rest = new Polinom();
		MonomReal m1 = new MonomReal(1,2);
		MonomReal m2 = new MonomReal((double)2/3,0);
		MonomReal m3 = new MonomReal((double)1/3,1);
		MonomReal m4 = new MonomReal((double)7/3,0);
		catAsteptat.adaugaMonom(m1);
		catAsteptat.adaugaMonom(m2);
		restAsteptat.adaugaMonom(m3);
		restAsteptat.adaugaMonom(m4);
		Polinom rezultat = Operatie.Impartire(P, Q, rest);
		assertTrue(rezultat.comparaPolinom(catAsteptat)); // verifica daca catul este corect
		assertTrue(rest.comparaPolinom(restAsteptat)); // verifica daca restul este corect
		nrTesteCuSucces++;
	}
	
	@Test
	public void testDerivare() {
		Polinom asteptat1 = new Polinom();
		Polinom asteptat2 = new Polinom();
		Monom m1 = new MonomIntreg(12,3);
		Monom m2 = new MonomIntreg(12,2);
		Monom m3 = new MonomIntreg(3,0);
		Monom m4 = new MonomIntreg(6,1);
		Monom m5 = new MonomIntreg(4,0);
		asteptat1.adaugaMonom(m1);
		asteptat1.adaugaMonom(m2);
		asteptat1.adaugaMonom(m3);
		asteptat2.adaugaMonom(m4);
		asteptat2.adaugaMonom(m5);
		Polinom rezultat1 = Operatie.Derivare(P);
		Polinom rezultat2 = Operatie.Derivare(Q);
		assertTrue(rezultat1.comparaPolinom(asteptat1)); // verifica daca derivarea lui P este corecta
		assertTrue(rezultat2.comparaPolinom(asteptat2)); // verifica daca derivarea lui Q este corecta
		nrTesteCuSucces++;
	}
	
	@Test
	public void testIntegrare() {
		Polinom asteptat1 = new Polinom();
		Polinom asteptat2 = new Polinom();
		MonomReal m1 = new MonomReal((double)3/5,5);
		MonomReal m2 = new MonomReal(1,4);
		MonomReal m3 = new MonomReal((double)3/2,2);
		MonomReal m4 = new MonomReal(1,1);
		MonomReal m5 = new MonomReal(1,3);
		MonomReal m6 = new MonomReal(2,2);
		MonomReal m7 = new MonomReal(-2,1);
		asteptat1.adaugaMonom(m1);
		asteptat1.adaugaMonom(m2);
		asteptat1.adaugaMonom(m3);
		asteptat1.adaugaMonom(m4);
		asteptat2.adaugaMonom(m5);
		asteptat2.adaugaMonom(m6);
		asteptat2.adaugaMonom(m7);
		Polinom rezultat1 = Operatie.Integrare(P);
		Polinom rezultat2 = Operatie.Integrare(Q);
		assertTrue(rezultat1.comparaPolinom(asteptat1)); // verifica daca integrarea lui P este corecta
		assertTrue(rezultat2.comparaPolinom(asteptat2)); // verifica daca integrarea lui Q este corecta

		nrTesteCuSucces++;
	}
	
}
