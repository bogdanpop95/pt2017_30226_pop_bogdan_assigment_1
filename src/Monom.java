// clasa Monom abstracta, defineste conceptul de grad

public abstract class Monom implements Comparable<Monom>{
	private int grad;
	
	public Monom(int grad) {
		this.grad = grad;
	}
	
	public int getGrad() {
		return grad;
	}
	
	public void setGrad(int grad) {
		this.grad = grad;
	}
	
	public abstract void setCoeficient(Number coeficient);
	public abstract Number getCoeficient();
		
	public int compareTo(Monom m) {
		if (grad > m.getGrad())
			return -1;
		else
			if(grad == m.getGrad())
				return 0;
			else
				return 1;
	}
	
	public abstract String toString(int gradMax);
}
