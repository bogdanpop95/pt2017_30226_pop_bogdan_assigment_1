// GUI
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class CalculatorPolinoame implements ActionListener{
	
	private static Polinom P, Q, R, W;
	JFrame appFrame;
	JLabel jlbFirstPolinom, jlbSecondPolinom, jlbResult, jlbP, jlbQ, jlbR, jlbCat, jlbRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, jlbValue; 
	JTextField jtfFirstPolinom, jtfSecondPolinom, jtfResult, jtfCat, jtfRest, jtfIn, jtfOut1, jtfOut2;
	JButton jbnAdd, jbnSubstract, jbnMultiply, jbnDivide, jbnDeriv, jbnInteg, jbnOk1, jbnOk2, jbnClear, jbnCalc;	
	Container cPane;
	JPanel pol1Panel, pol2Panel, resultPanel;
	
	public CalculatorPolinoame() {
		prepareGUI();
	}
	
	private void prepareGUI() {
		// Cream un frame, si ii setam Panelul content
   		appFrame = new JFrame("Calculator - Polinoame");
   		appFrame.setSize(700, 400);
   		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   		cPane = appFrame.getContentPane();
   		cPane.setLayout(null);

   		arrangeComponents();
   		appFrame.setResizable(false);
   		appFrame.setVisible(true);
	}
	
	private void arrangeComponents() {
		// Primul polinom, intr-un panel bordat
		
		pol1Panel = new JPanel();
		pol1Panel.setBounds(10, 10, 325, 80);
		pol1Panel.setBorder(new TitledBorder(new EtchedBorder(),"Introduceti primul polinom"));

		jlbP = new JLabel("P(x) = ");
		jlbP.setBounds(20, 40, 40, 20);
		cPane.add(jlbP);
		
		jtfFirstPolinom = new JTextField();
		jtfFirstPolinom.setBounds(60, 40, 200, 20);
		cPane.add(jtfFirstPolinom);
		jbnOk1 = new JButton("OK");
		jbnOk1.setBounds(265, 40, 60, 20);
		jbnOk1.addActionListener(this);
		cPane.add(jbnOk1);

		cPane.add(pol1Panel);
	
		// Butoane cu operatii
		
		jbnAdd = new JButton(new ImageIcon("D:/Downloads/my-icons-collection/png/005-mathematical-addition-sign.png"));
		jbnAdd.setBounds(10, 100, 50, 50);
		jbnAdd.setBorder(new EtchedBorder());
		jbnAdd.addActionListener(this);
		jbnSubstract = new JButton(new ImageIcon("D:/Downloads/my-icons-collection/png/003-minus-sign.png"));
		jbnSubstract.setBounds(65, 100, 50, 50);
		jbnSubstract.setBorder(new EtchedBorder());
		jbnSubstract.addActionListener(this);
		jbnMultiply = new JButton(new ImageIcon("D:/Downloads/my-icons-collection/png/002-multiply-mathematical-sign.png"));
		jbnMultiply.setBounds(120, 100, 50, 50);
		jbnMultiply.setBorder(new EtchedBorder());
		jbnMultiply.addActionListener(this);
		jbnDivide = new JButton(new ImageIcon("D:/Downloads/my-icons-collection/png/004-divide-mathematical-sign.png"));
		jbnDivide.setBounds(175, 100, 50, 50);
		jbnDivide.setBorder(new EtchedBorder());
		jbnDivide.addActionListener(this);
		jbnDeriv = new JButton("'",new ImageIcon("D:/Downloads/my-icons-collection (1)/png/001-sign.png"));
		jbnDeriv.setBounds(230, 100, 50, 50);
		jbnDeriv.setBorder(new EtchedBorder());
		jbnDeriv.addActionListener(this);
		jbnInteg = new JButton(new ImageIcon("D:/Downloads/my-icons-collection/png/006-integral-mathematical-sign.png"));
		jbnInteg.setBounds(285, 100, 50, 50);
		jbnInteg.setBorder(new EtchedBorder());
		jbnInteg.addActionListener(this);
		
		cPane.add(jbnAdd);
		cPane.add(jbnSubstract);
		cPane.add(jbnMultiply);
		cPane.add(jbnDivide);
		cPane.add(jbnDeriv);
		cPane.add(jbnInteg);
		
		// Cel de-al doilea polinom, intr-un panel bordat
		pol2Panel = new JPanel();
		pol2Panel.setBounds(10, 170, 325, 80);
		pol2Panel.setBorder(new TitledBorder(new EtchedBorder(),"Introduceti cel de-al doilea polinom"));

		jlbQ = new JLabel("Q(x) = ");
		jlbQ.setBounds(20, 200, 40, 20);
		cPane.add(jlbQ);
		
		jtfSecondPolinom = new JTextField();
		jtfSecondPolinom.setBounds(60, 200, 200, 20);
		cPane.add(jtfSecondPolinom);
		jbnOk2 = new JButton("OK");
		jbnOk2.setBounds(265, 200, 60, 20);
		jbnOk2.addActionListener(this);
		cPane.add(jbnOk2);
		cPane.add(pol2Panel);
		
		// Polinomul rezultat / Polinoamele rezultate intr-un Panel rezultat
		resultPanel = new JPanel();
		resultPanel.setBounds(340, 90, 300, 80);
		resultPanel.setBorder(new TitledBorder(new EtchedBorder(),"Rezultatul operatiei alese"));
		
		jlbR = new JLabel("R(x) = ");
		jlbR.setBounds(350, 120, 40, 20);
		cPane.add(jlbR);
		
		jtfResult = new JTextField();
		jtfResult.setBounds(390, 120, 200, 20);
		cPane.add(jtfResult);

		jlbCat = new JLabel("Cat(x)   = ");
		jlbCat.setBounds(345, 115, 60, 20);
		cPane.add(jlbCat);
		jlbCat.setVisible(false);
		
		jtfCat = new JTextField();
		jtfCat.setBounds(410, 115, 200, 20);
		cPane.add(jtfCat);
		jtfCat.setVisible(false);
		cPane.add(resultPanel);
		
		jlbRest = new JLabel("Rest(x) = ");
		jlbRest.setBounds(345, 135, 60, 20);
		cPane.add(jlbRest);
		jlbRest.setVisible(false);
		
		jtfRest = new JTextField();
		jtfRest.setBounds(410, 135, 200, 20);
		cPane.add(jtfRest);
		jtfRest.setVisible(false);
		cPane.add(resultPanel);
		
		jlbDeriv1 = new JLabel("P'(x) = ");
		jlbDeriv1.setBounds(350, 115, 60, 20);
		cPane.add(jlbDeriv1);
		jlbDeriv1.setVisible(false);
		
		jlbDeriv2 = new JLabel("Q'(x) = ");
		jlbDeriv2.setBounds(350, 135, 60, 20);
		cPane.add(jlbDeriv2);
		jlbDeriv2.setVisible(false);
		cPane.add(resultPanel);
		
		jlbInteg1 = new JLabel((char)(8747) + "P(x) = ");
		jlbInteg1.setBounds(350, 115, 60, 20);
		cPane.add(jlbInteg1);
		jlbInteg1.setVisible(false);
		
		jlbInteg2 = new JLabel((char)(8747) + "Q(x) = ");
		jlbInteg2.setBounds(350, 135, 60, 20);
		cPane.add(jlbInteg2);
		jlbInteg2.setVisible(false);
		cPane.add(resultPanel);
		
		jbnClear = new JButton("CLEAR");
		jbnClear.setBounds(430, 200, 90, 40);
		jbnClear.addActionListener(this);
		cPane.add(jbnClear);

		jlbValue = new JLabel("Valoarea polinoamelor in punctul x = ");
		jlbValue.setBounds(50, 270, 250, 20);
		cPane.add(jlbValue);
		
		jtfIn = new JTextField();
		jtfIn.setBounds(260, 270, 40, 20);
		cPane.add(jtfIn);
		
		jbnCalc = new JButton("Calculeaza");
		jbnCalc.setBounds(310, 260, 100, 40);
		jbnCalc.addActionListener(this);
		cPane.add(jbnCalc);
		
		jlbP = new JLabel("P(value)=");
		jlbP.setBounds(50, 310, 80, 20);
		cPane.add(jlbP);
		
		jlbQ = new JLabel("Q(value)=");
		jlbQ.setBounds(50, 330, 80, 20);
		cPane.add(jlbQ);
		
		jtfOut1 = new JTextField();
		jtfOut1.setBounds(110, 310, 200, 20);
		cPane.add(jtfOut1);
		
		jtfOut2 = new JTextField();
		jtfOut2.setBounds(110, 330, 200, 20);
		cPane.add(jtfOut2);
		
	}
	//setam panel-ul potrivit pentru rezultat
	
	private void setPanel(JLabel jlbR, JTextField jtfResult, JLabel jlbCat,JTextField jtfCat, JLabel jlbRest, JTextField jtfRest, JLabel jlbDeriv1, JLabel jlbDeriv2, JLabel jlbInteg1, JLabel jlbInteg2, boolean bool1, boolean bool2, boolean bool3, boolean bool4) {
		jlbR.setVisible(bool1); 
		jtfResult.setVisible(bool1); 
		jlbCat.setVisible(bool2); 
		jtfCat.setVisible(bool2 || bool3 || bool4);
		jlbRest.setVisible(bool2);
		jtfRest.setVisible(bool2 || bool3 || bool4);
		jlbDeriv1.setVisible(bool3);
		jlbDeriv2.setVisible(bool3);
		jlbInteg1.setVisible(bool4);
		jlbInteg2.setVisible(bool4);
	}
	
	// Verificam apasarea unui anumit buton si efectuam actiunea dorita
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jbnOk1) {
			String pol1 = jtfFirstPolinom.getText();
			P = new Polinom();
			citestePolinom(pol1,P);
			jtfFirstPolinom.setText(P.toString());
			System.out.println(P);
		} else if(e.getSource() == jbnOk2) {
			String pol2 = jtfSecondPolinom.getText();
			Q = new Polinom();
			citestePolinom(pol2,Q);
			jtfSecondPolinom.setText(Q.toString());
			System.out.println(Q);
		} else if(e.getSource() == jbnAdd) {
			setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, true, false, false, false);
			R = Operatie.Adunare(P, Q);
			jtfResult.setText(R.toString());
		} else if(e.getSource() == jbnSubstract) {
			 setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, true, false, false, false);
			R = Operatie.Scadere(P, Q);
			jtfResult.setText(R.toString());
		} else if(e.getSource() == jbnMultiply) {
			setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, true, false, false, false);
			R = Operatie.Inmultire(P, Q);
			jtfResult.setText(R.toString());
		} else if(e.getSource() == jbnDivide) {
			setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest,jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, false, true, false, false);
			Polinom rest = new Polinom();
			R = Operatie.Impartire(P, Q, rest);
			jtfCat.setText(R.toString());
			jtfRest.setText(rest.toString());
		} else if(e.getSource() == jbnDeriv) {
			setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, false, false, true, false);
			R = Operatie.Derivare(P);
			W = Operatie.Derivare(Q);
			jtfCat.setText(R.toString());
			jtfRest.setText(W.toString());
		} else if(e.getSource() == jbnInteg) {
			setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, false, false, false, true);
			R = Operatie.Integrare(P);
			W = Operatie.Integrare(Q);
			jtfCat.setText(R.toString());
			jtfRest.setText(W.toString());
		} else if(e.getSource() == jbnCalc) {
			try {
				jtfOut1.setText(Integer.toString(P.getValoare(Integer.parseInt(jtfIn.getText()))));
				jtfOut2.setText(Integer.toString(Q.getValoare(Integer.parseInt(jtfIn.getText()))));
			} catch(NumberFormatException r) {
				JOptionPane.showMessageDialog(appFrame,"Ați introdus un format invalid!", "Error!", JOptionPane.ERROR_MESSAGE);
			}

		} else if(e.getSource() == jbnClear) {
			if(P != null) {
				P.resetPolinom(); jtfFirstPolinom.setText(""); jtfOut1.setText("");
			}
			if(Q != null) {
				Q.resetPolinom(); jtfSecondPolinom.setText(Q.toString()); jtfOut2.setText("");
			}
			if(R != null) {
				R.resetPolinom(); jtfResult.setText(R.toString());
			}
			if(W != null) {
				W.resetPolinom(); jtfRest.setText(W.toString());
			}
			setPanel(jlbR, jtfResult, jlbCat, jtfCat, jlbRest, jtfRest, jlbDeriv1, jlbDeriv2, jlbInteg1, jlbInteg2, true, false, false, false);
		}
		
	}
	
	private void citestePolinom(String pol, Polinom P) {
		// separam in monoame
		String[] buffer = pol.replace(" ", "").split("(?=\\+|\\-)");
		
		for(String term : buffer) {
			Monom monom = new MonomIntreg(0,0);
			if(validate(term, monom))
				P.adaugaMonom(monom);
			else
				JOptionPane.showMessageDialog(appFrame,"Ați introdus un format invalid!", "Error!", JOptionPane.ERROR_MESSAGE);
		}
		P.initPolinom();
	}
	
	private boolean validate(String m, Monom monom) {
		// verificam daca monomul nu prezinta formatul potrivit(pattern-ul expresiei regulate), altfel prelucram monomul
		if(!m.matches("[a-z0-9\\-\\*\\^\\+]*"))
			return false;
		String[] splitAroundX = m.split("x", 2);
		int exponent = 0, coeficient = 0;
		if(splitAroundX.length > 1) {
			String sExp = splitAroundX[1].replace("^", "");
			exponent = sExp.isEmpty() ? 1 : Integer.parseInt(sExp);
		}
		String sCoef = splitAroundX[0];
		try {
			coeficient = sCoef.isEmpty() ? 1 : ("+".equals(sCoef) ? Integer.parseInt(sCoef.replace("+", "1")) : ("-".equals(sCoef) ? -1 : Integer.parseInt(sCoef)));
		}
		catch (NumberFormatException e) {
			return false;
		}
		monom.setGrad(exponent);
		monom.setCoeficient(coeficient);
		return true;
	}
}
