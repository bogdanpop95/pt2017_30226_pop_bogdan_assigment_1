// clasa Polinom, implementeaza un sir de Monom pe care sunt posibile operatii
import java.util.*;

public class Polinom {
	private ArrayList<Monom> polinom =new ArrayList<Monom>();
	private int valoare = 0;
	private int gradPol;
	
	public void adaugaMonom(Monom monom) {
		boolean flag = true;
		for(Monom mon: polinom) {
			if(mon.getGrad() == monom.getGrad()) {
				mon.setCoeficient(mon.getCoeficient().doubleValue() + monom.getCoeficient().doubleValue());
				flag = false;
			}	
		}
		if(flag)
			polinom.add(monom);
	}
	
	public void stergeMonom(Monom monom) {
		polinom.remove(monom);
	}
	// initializeaza Polinomul, adica il ordoneaza descrescator dupa grad si seteaza gradul polinomului si mai mult de atat, elimina Monoamele care dupa adaugare sunt invalide (coeficient = 0)
	
	public void initPolinom() {
		for(int i=0; i< polinom.size(); i++) {
			if(polinom.get(i).getCoeficient().doubleValue() == 0)
				polinom.remove(i);
		}
		if(polinom.size() != 0) {
			Collections.sort(polinom);
			gradPol = polinom.get(0).getGrad();
		}
	}
	
	
	public int getValoare(int x) {
		valoare = 0;
		for(Monom mon: polinom) {
			valoare += ( Math.pow(x, mon.getGrad()) * mon.getCoeficient().doubleValue());
		}
		return valoare;
	}
	
	public int getGrad() {
		return gradPol;
	}
	
	public ArrayList<Monom> getPolinom() {
		return polinom;
	}
	
	public void resetPolinom() {
			valoare = 0;
			gradPol = 0;
			polinom.clear();

	}
	
	public boolean comparaPolinom(Polinom polDeComparat) {
		double ERR = 0.0001;
		polDeComparat.initPolinom();
		if (this.gradPol != polDeComparat.gradPol)
			return false;
		for(int i=0; i < this.getPolinom().size(); i++) {
			if(polinom.get(i).getGrad() != polDeComparat.getPolinom().get(i).getGrad())
				return false;
			if(Math.abs(polinom.get(i).getCoeficient().doubleValue() - polDeComparat.getPolinom().get(i).getCoeficient().doubleValue()) > ERR)
				return false;
		}
		return true;
	}
	
	public String toString() {
		String s = "";
		for(Monom mon: polinom) {
			s+= mon.toString(gradPol);
		}
		return s;
	}
}
