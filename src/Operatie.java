
public class Operatie {
	
	// La fiecare din urmatoarele operatii vom crea un polinom nou rezultat
	// (implicit monoame noi, pentru a nu afecta monoamele initiale) care va fi returnat
	
	public static Polinom Adunare(Polinom P, Polinom Q) {
		Polinom R = new Polinom();
		for(Monom m1 : P.getPolinom()) {
			R.adaugaMonom(new MonomIntreg(m1.getCoeficient().intValue(), m1.getGrad()));
		}
		for(Monom m2: Q.getPolinom()) {
			R.adaugaMonom(new MonomIntreg(m2.getCoeficient().intValue(), m2.getGrad()));
		}
		R.initPolinom();
		return R;
	}
	
	public static Polinom Scadere(Polinom P, Polinom Q) {
		Polinom R = new Polinom();
		for(Monom m1 : P.getPolinom()) {
			R.adaugaMonom(new MonomIntreg(m1.getCoeficient().intValue(), m1.getGrad()));
		}
		for(Monom m2: Q.getPolinom()) {
			R.adaugaMonom(new MonomIntreg(-m2.getCoeficient().intValue(), m2.getGrad()));
		}
		R.initPolinom();
		return R;
	}
	
	public static Polinom Inmultire(Polinom P, Polinom Q) {
		Polinom R = new Polinom();
		for(Monom m1 : P.getPolinom()){
			for(Monom m2 : Q.getPolinom()) {
				R.adaugaMonom(new MonomIntreg(m1.getCoeficient().intValue() * m2.getCoeficient().intValue(), m1.getGrad() + m2.getGrad()));
			}
		}
		R.initPolinom();
		return R;
	}
	
	public static Polinom Impartire(Polinom P, Polinom Q, Polinom rest) {
		Polinom cat = new Polinom();				// avem nevoie de 2 polinoame reale pentru a efectua calculele si a nu strica polinoamele initiale
		if(P.getGrad() - Q.getGrad() < 0)					// daca gradul lui P mai mic decat gradul lui Q rezultatul este 0																				
			return null;
		
		//copiem polinomul deimparit, din care se va afla restul, la fiecare pas ulterior(la fiecare gasire a unui monom al catului)
		
		for(Monom p : P.getPolinom()) {						
			rest.adaugaMonom(new MonomReal(p.getCoeficient().doubleValue(), p.getGrad()));
		}
		rest.initPolinom();
		
		// Polinomul cat va avea grad = grad(P) - grad(Q); algoritmul va face grad(cat) + 1 pasi
		// La fiecare pas, monomul de grad maxim al catului se va inmulti cu monoamele lui Q si se va scadea din restul initial, formand noul rest;
		int i = P.getGrad()-Q.getGrad();
		while(rest.getPolinom().size() >0 && i >= 0) {
			MonomReal c = new MonomReal(rest.getPolinom().get(0).getCoeficient().doubleValue() / Q.getPolinom().get(0).getCoeficient().doubleValue(), i);
			cat.adaugaMonom(c);
			for(Monom q : Q.getPolinom()) {
				rest.adaugaMonom(new MonomReal(-c.getCoeficient().doubleValue() * q.getCoeficient().doubleValue(), c.getGrad() + q.getGrad()));
			}
			rest.initPolinom();
			i--;
		}
		cat.initPolinom();
		return cat;
	}
	
	public static Polinom Derivare(Polinom P) {
		Polinom R = new Polinom();
		for(Monom p : P.getPolinom()) {
			R.adaugaMonom(new MonomIntreg(p.getCoeficient().intValue() * p.getGrad(), p.getGrad() - 1));
		}
		R.initPolinom();
		return R;
	}
	
	public static Polinom Integrare(Polinom P) {
		Polinom R = new Polinom();
		for(Monom p : P.getPolinom()) {
			R.adaugaMonom(new MonomReal(p.getCoeficient().doubleValue() / (double)(p.getGrad() + 1), p.getGrad() + 1));
		}
		R.initPolinom();
		return R;
	}
}
